import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class Car {
    private int x;
    private int y;

    public Car(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void draw(Graphics2D g2){
        Rectangle body = new Rectangle(x, y+10, 60, 10);
        Ellipse2D.Double frontTire = new Ellipse2D.Double(x + 10, y + 20, 10,10);
        Ellipse2D.Double rearTire = new Ellipse2D.Double(x + 40, y + 20, 10,10);

        Point2D.Double r1 = new Point2D.Double(x + 10, y + 10);
        Point2D.Double r2 = new Point2D.Double(x + 20, y);
        Point2D.Double r3 = new Point2D.Double(x + 40, y);
        Point2D.Double r4 = new Point2D.Double(x + 50, y + 10);

        Line2D.Double frontWindshield = new Line2D.Double(r1,r2);
        Line2D.Double roofTop = new Line2D.Double(r2, r3);
        Line2D.Double rearWindShield = new Line2D.Double(r3,r4);
        g2.draw(body);
        g2.draw(frontTire);
        g2.draw(rearTire);
        g2.draw(frontWindshield);
        g2.draw(roofTop);
        g2.draw(rearWindShield);
    }
    public void translate(int dx, int dy){
        x += dx;
        y += dy;
    }
}
