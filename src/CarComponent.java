import javax.swing.JComponent;
import javax.swing.Timer;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CarComponent extends JComponent {
    int x1 = 0;
    int y1 = 0;
    Timer timer;
    public CarComponent() {
        // initializing timer
        timer = new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // moving first car to right and second to left
                x1++;
                y1++;
                // updating window
                repaint();
            }
        });
        // using a delay of 50 ms
        timer.setDelay(50);
        // starting timer
        timer.start();
    }
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        // also updating coordinates of first car if it goes out of window
        if (x1 > getWidth()) {
            x1 = y1 = 0;
        }

        // creating car1 with x1, y1 and car2 with x2, y2
        Car car1 = new Car(x1, y1);

        car1.draw(g2);

    }
}
